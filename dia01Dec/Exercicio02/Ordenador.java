package Exercicio02;
    

    class Ordenador {
        private String marca; //marca del ordenador
        private String procesador; //marca del procesador
        private float peso; //peso del ordenador
        private boolean encendido; //si vale true => está encendido. si vale false => está apagado
        private boolean pantalla; //si vale true => está activada. si vale false => está desactivada
        
        public String obtenerMarca() {
        return marca;
        }
        public void establecerMarca(String ma) {
        marca = ma;
        }
       
        public void encenderOrdenador() {
        if (encendido == true) {
        System.out.println("El ordenador ya está encendido");
        }
        else {
        encendido = true;
        pantalla = true;
        System.out.println("El ordenador ha sido encendido");
        }
        }
       
        public void comprobarEstado() {
        System.out.print("\nEl estado del ordenador es el siguiente:");
        System.out.print("\nMarca: " + marca);
        System.out.print("\nProcesador: " + procesador);
        System.out.print("\nPeso: " + peso + " kg.");
        if (encendido == true) {
        System.out.print("\nEl ordenador está encendido");
        }
        else {
        System.out.print("\nEl ordenador está apagado");
        }
        if (pantalla == true) {
        System.out.print("\nLa pantalla está activada");
        }
        else {
        System.out.print("\nLa pantalla está desactivada");
        }
        System.out.println("\n");
        }
    
        //Desactivar pantalla

        public void DesactivarPantalla(){
            if(pantalla==false){
                System.out.println("La pantalla ya está desactivada");
            } else{
                pantalla=false;
                System.out.println("Apagar pantalla");
            }
        }

        public void ActivarPantalla(){
            if(pantalla==true){
                System.out.println("Pantalla ya activada");
            }else{
                pantalla=true;
                System.out.println("La pantalla ha sido activada");
            }
        }

        public void apagarOrdenador(){
            if(encendido==false){
                System.out.println("Ordenador apagado");
            }else{
            encendido=true;
            System.out.println("Apagar ordenador");
            }
        }

        //Ahora establece peso, obtenlo, establece tb procesador y obtenlo

        public void setPeso(float peso){
            if(peso<0){
                this.peso=0;
            }else {
          this.peso=peso;

            }

        }
        public float getPeso(){
            return peso;
        }

        public void setProcesador(String procesador){
            if(procesador==null){
                this.procesador= "Sin procesador";
            }
            else{
                this.procesador=procesador;
            }
        }
        
        public String getProcesador(){
            return procesador;
        }


    }










    


import java.time.LocalDate;
import java.time.Period;


public class Alumno {
    private String nombre;
    private Integer id;
    private LocalDate fechaNacimiento;
    private static Integer currentId = 1;


    public Alumno(){
        nombre = "Sin nombre";
        id = currentId;
        currentId++;
        fechaNacimiento = LocalDate.now();
    }

    public Alumno(String nombre, Integer edad, String fechaNacimiento){
        this.nombre = nombre;
        id = currentId;
        currentId++;
        this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId(){
        return id;
    }

    public Integer getEdadMal(){

        return LocalDate.now().getYear()-fechaNacimiento.getYear();
    }

    

    public boolean esCumpleaños(){
        if(LocalDate.now().getDayOfMonth() == fechaNacimiento.getDayOfMonth() 
        && LocalDate.now().getMonth() == fechaNacimiento.getMonth()){
            return true;
        }
        else{
            return false;
        }
    }

    public Integer getEdad(){
        return Period.between(fechaNacimiento, LocalDate.now()).getYears();
    }

    public String toString(){
        return "Nombre: " + " " + nombre + "\nID: " + id + "\nEdad: " + getEdad();
    }

    
}


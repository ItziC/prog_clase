public class Curso {
    private String nombre;
    private Alumno[] alumnos;
    private Integer cantidadAlumnos;

    public Curso(String nombre, Integer cantidadAlumnos) {
        this.nombre = nombre;
        this.cantidadAlumnos = 0;
        this.alumnos = new Alumno[cantidadAlumnos];
    }

    public void anadirAlumno(Alumno alumno) {
        if (cantidadAlumnos == alumnos.length) {
            System.out.println("El curso está lleno");
        } else {
            alumnos[this.cantidadAlumnos] = alumno;
            cantidadAlumnos++;
        }
    }

    public void eliminarAlumno(Integer posicion){
        if(posicion > alumnos.length){
            System.out.println("Posicion fuera de rango");
        }
        else{
            if(alumnos[posicion-1] != null){
                alumnos[posicion-1] = null;
                cantidadAlumnos--;
            }
        }
    }

    public void eliminarTodosAlumnos(){
        for(int i = 0; i < alumnos.length ; i++){
            alumnos[i] = null;
            
        }
        cantidadAlumnos = 0;
        
    }
}
